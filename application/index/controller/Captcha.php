<?php

namespace app\index\controller;

use think\Controller;
use Gregwar\Captcha\CaptchaBuilder;
use think\facade\Session;

class Captcha extends Controller
{

    public function index()
    {
        $builder = new CaptchaBuilder;
        putenv('GDFONTPATH=' . realpath(app()->getRootPath() . '/vendor/gregwar/captcha/src/Gregwar/Captcha/Font'));
        $builder->build(150, 36, 'captcha' . mt_rand(0, 5) . '.ttf');
        header('Content-type: image/jpeg');

        Session::set('captcha', strtolower($builder->getPhrase()));

        return $builder->output();
    }

}
