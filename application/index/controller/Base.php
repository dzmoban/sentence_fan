<?php

namespace app\index\controller;

use think\Controller;
use think\Request;

class Base extends Controller
{

    // 表单验证抽离
    protected function validator($data, $validator)
    {
        $validatorObj = new $validator;
        if (! $validatorObj->check($data)) {
            $this->error($validatorObj->getError());
        }
    }
}
