<?php
namespace app\index\controller;

use app\common\model\Sentence;
use think\facade\Session;

class Index
{
    public function index()
    {
        $sentences = Sentence::order('create_time', 'desc')->paginate(12);

        return view('', compact('sentences'));
    }
}
