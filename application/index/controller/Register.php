<?php

namespace app\index\controller;

use app\common\model\User;
use app\common\validate\UserValidate;
use think\Request;

class Register extends Base
{

    public function index()
    {
        return view();
    }

    public function registerHandler(Request $request)
    {
        $user = $request->only(['username', 'password', 'password_confirm']);
        $validator = new UserValidate;
        ! $validator->check($user) && $this->error($validator->getError());

        (new User)->save($user);

        $this->success('注册成功', '/');
    }

}
