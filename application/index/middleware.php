<?php

return [
    \app\http\middleware\RegisterAuthToContainerMiddleware::class,
    \app\http\middleware\LoginCheckMiddleware::class,
];