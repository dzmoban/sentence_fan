<?php

namespace app\common\validate;

use think\Validate;

class PasswordChangeValidate extends Validate
{
	protected $rule = [
	    'password|密码' => 'require|min:6|max:16|confirm:password_confirm'
    ];

    protected $message = [];
}
