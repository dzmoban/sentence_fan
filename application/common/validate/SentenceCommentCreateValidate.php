<?php

namespace app\common\validate;

use think\Validate;

class SentenceCommentCreateValidate extends Validate
{
	protected $rule = [
	    'content|评论' => 'require|min:10|max:255',
    ];
}
