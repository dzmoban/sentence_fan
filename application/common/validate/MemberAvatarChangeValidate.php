<?php

namespace app\common\validate;

use think\Validate;

class MemberAvatarChangeValidate extends Validate
{
	protected $rule = [
	    'file' => 'require|image|fileSize:1048576'
    ];

    protected $message = [
        'file.require' => '请上传头像',
        'file.image' => '请上传有效的图片文件',
        'file.fileSize' => '头像大小不能超过1024kb',
    ];
}
