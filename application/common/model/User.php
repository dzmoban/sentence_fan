<?php

namespace app\common\model;

use think\helper\Hash;
use think\Model;

class User extends Model
{

    protected $table = 'users';

    protected $field = [
        'id', 'username', 'password', 'create_time', 'update_time',
        'email', 'avatar',
    ];

    public function sentences()
    {
        return $this->hasMany(Sentence::class, 'user_id');
    }

    public function comments()
    {
        return $this->hasMany(SentenceComment::class, 'user_id');
    }

    public function setPasswordAttr($password)
    {
        return Hash::make($password);
    }

}
