<?php

namespace app\common\model;

use think\Model;

class Tag extends Model
{

    protected $table = 'tags';

    protected $field = ['name', 'category_id'];

    public function sentences()
    {
        return $this->belongsToMany(Sentence::class, 'sentence_tag', 'sentence_id', 'tag_id');
    }

    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id');
    }

}
