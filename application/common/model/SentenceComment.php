<?php

namespace app\common\model;

use think\Model;

class SentenceComment extends Model
{

    protected $table = 'sentence_comments';

    protected $field = [
        'user_id', 'sentence_id', 'content',
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function sentence()
    {
        return $this->belongsTo(Sentence::class, 'sentence_id');
    }

}
