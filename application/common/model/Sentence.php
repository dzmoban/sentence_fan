<?php

namespace app\common\model;

use think\Model;

class Sentence extends Model
{

    protected $table = 'sentences';

    protected $field = [
        'id', 'sentence', 'create_time', 'update_time',
        'user_id',
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function comments()
    {
        return $this->hasMany(SentenceComment::class, 'sentence_id');
    }

    public function tags()
    {
        return $this->belongsToMany(Tag::class, 'sentence_tag', 'tag_id', 'sentence_id');
    }

}
