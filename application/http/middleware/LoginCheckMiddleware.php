<?php

namespace app\http\middleware;

use app\common\library\Auth;
use think\facade\Session;

class LoginCheckMiddleware
{
    public function handle($request, \Closure $next)
    {
        // /member*
        if (
            preg_match('/\/member*/', $request->server('REQUEST_URI')) &&
            ! Auth::getInstance()->check()
        ) {
            Session::set('login_referer', $request->server('REQUEST_URI'));
            return redirect('/login');
        }
        return $next($request);
    }
}
