<?php

namespace app\http\middleware;

use app\common\library\Auth;
use think\Container;
use think\facade\View;

class RegisterAuthToContainerMiddleware
{
    public function handle($request, \Closure $next)
    {
        $auth = Auth::getInstance();
        Container::set('auth', $auth);
        View::share('user', $auth->check() ? $auth->user() : null);
        return $next($request);
    }
}
