<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2018 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------

Route::get('/', 'index/index/index');

Route::get('/register', 'index/register/index');
Route::post('/register', 'index/register/registerHandler')->middleware('captcha.check');

Route::get('/login', 'index/login/index');
Route::post('/login', 'index/login/loginHandler')->middleware('captcha.check');

Route::get('/logout', 'index/login/logout');

// 验证码图片路由
Route::get('/captcha', 'index/captcha/index');

// 找回密码
Route::get('/password', 'index/password/index');
Route::post('/password', 'index/password/handler')->middleware('captcha.check');
Route::get('/password/find', 'index/password/showPasswordChangePage');
Route::post('/password/find', 'index/password/passwordChangeHandler');

// 用户中心
Route::get('/member', 'index/member/index');
Route::get('/member/password_change', 'index/member/showChangePasswordPage');
Route::post('/member/password_change', 'index/member/changePasswordHandler');
Route::get('/member/avatar', 'index/member/showAvatarChangePage');
Route::post('/member/avatar', 'index/member/avatarChangeHandler');
Route::get('/member/sentences', 'index/member/showSentencesPage');
Route::get('/member/sentences/comment', 'index/member/showSentencesCommentPage');

// 句子
Route::get('/sentence/:id', 'index/sentence/show');
Route::get('/member/sentence/create', 'index/sentence/create');
Route::post('/member/sentence/create', 'index/sentence/store');
Route::post('/member/sentence/:sid/comment', 'index/sentence/commentHandler');
Route::get('/member/sentence/:id/edit', 'index/sentence/edit');
Route::post('/member/sentence/:id/edit', 'index/sentence/update');
Route::get('/member/sentence/:id/destroy', 'index/sentence/destroy');