<?php

use think\migration\Migrator;
use think\migration\db\Column;

class AddCategoryIdForTagsTable extends Migrator
{
    public function up()
    {
        $categoryId = (new Column)->setName('category_id')->setType('integer')->setComment('分类ID')->setDefault(0);
        $this->table('tags')->addColumn($categoryId)->save();
    }

    public function down()
    {
        $this->table('tags')->removeColumn('category_id');
    }
}
