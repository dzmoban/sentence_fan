<?php

use think\migration\Migrator;
use think\migration\db\Column;

class AddEmailColumnForUsersTable extends Migrator
{

    public function up()
    {
        $table = $this->table('users');

        $emailColumn = (new Column())->setName('email')->setType('string')->setLimit(64)->setDefault('')->setComment('邮箱')->setAfter('password');

        $table->addColumn($emailColumn)->save();
    }

    public function down()
    {
        $table = $this->table('users');
        if ($table->hasColumn('email')) {
            $table->removeColumn('email');
        }
    }

}
