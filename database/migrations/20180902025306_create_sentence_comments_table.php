<?php

use think\migration\Migrator;
use think\migration\db\Column;

class CreateSentenceCommentsTable extends Migrator
{

    public function up()
    {
        $userIdColumn = (new Column)->setName('user_id')->setType('integer')->setComment('用户ID');
        $sentenceIdColumn = (new Column)->setName('sentence_id')->setType('integer')->setComment('句子ID');
        $contentColumn = (new Column)->setName('content')->setType('string')->setLimit(255)->setComment('评论内容');
        $this->table('sentence_comments')
            ->addColumn($userIdColumn)
            ->addColumn($sentenceIdColumn)
            ->addColumn($contentColumn)
            ->addTimestamps()
            ->create();
    }

    public function down()
    {
        $this->dropTable('sentence_comments');
    }

}
