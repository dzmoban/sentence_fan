<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/8/12
 * Time: 19:01
 */

namespace tests;

use app\common\model\User;

class RegisterPageTest extends TestCase
{

    static $userData;
    static $user;

    public static function setUpBeforeClass()
    {
        $data = [
            'username' => bin2hex(random_bytes(8)),
            'password' => '123456',
        ];
        self::$user = new User($data);
        self::$user->save();

        self::$userData = array_merge($data, ['password_confirm' => $data['password']]);
    }

    public static function tearDownAfterClass()
    {
        self::$user->delete();
    }

    public function test_visit_register_page()
    {
        $this->visit('/register')->see('注册');
    }

    public function test_require_input_username()
    {
        $this->visit('/register')->submitForm('点我注册')->see('用户名不能为空');
    }

    public function test_require_username_length_mt_two()
    {
        $this->visit('/register')->submitForm('点我注册', ['username' => '1'])->see('用户名长度不能小于 2');
    }

    public function test_require_username_length_lt_16()
    {
        $this->visit('/register')->submitForm('点我注册', ['username' => bin2hex(random_bytes(48))])->see('用户名长度不能超过 16');
    }

    public function test_require_username_not_exists()
    {
        $this->visit('/register')
            ->submitForm('点我注册', self::$userData)
            ->see('用户名已存在');
    }

    public function test_require_input_password()
    {
        $this->visit('/register')
            ->submitForm('点我注册', [
                'username' => bin2hex(random_bytes(8)),
            ])
            ->see('密码不能为空');
    }

}